# Grab On Utilities

Copyright © 2022 Magnileve, released under the MIT License.

This is a collection of utilities to be used while creating Grab On maps.
Maps are loaded using the [Grab On Loader](https://codeberg.org/GrabOn/GrabOnLoader).

## How to Use

Download and run the jar file from the latest release.
Make sure Java 17 or a newer version is installed.
In order to set a property, make sure to press `Enter` after typing a value in its text field.

### How to Build

Download the source code, open the project directory in the terminal/command prompt, and enter `./gradlew build` (on Windows, omit the `./`).  The jar file will be in `build/libs`.

## Current Utilities

Unless otherwise noted, input files are found from `fromDir`, and result files are output to `toDir`.

#### Add stex png files

Searches for any stex files imported by Godot from a png file and changes their names to their original names but with the stex extension

#### Resize pngs

Modifies all png images of a map layer determined by `layer` to have a new size determined by `length` on `axis`.

#### Delete empty pngs

Deletes any blank png images in `toDir`.

#### Create polygons from svgs

Scans svg files for paths, converts their format, and adds them to `polygons.txt`
