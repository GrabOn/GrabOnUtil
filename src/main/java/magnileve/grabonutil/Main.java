package magnileve.grabonutil;

import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import ar.com.hjg.pngj.ImageLineInt;
import ar.com.hjg.pngj.PngReader;

public class Main {

private static final String PROPERTIES_PATH;

static {
	String os = System.getProperty("os.name");
	String user = System.getProperty("user.home");
	String file = "grabonutil.properties";
	if(os.startsWith("Linux")) PROPERTIES_PATH = user + "/.local/share/grabon/" + file;
	else if(os.startsWith("Windows")) PROPERTIES_PATH = user + "/AppData/Roaming/grabon/" + file;
	else if(os.startsWith("Mac")) PROPERTIES_PATH = user + "/Library/Application Support/grabon/" + file;
	else PROPERTIES_PATH = file;
}

private final JFrame frame;
private final JLabel logDisplay;

private final Properties defaultProperties = new Properties();
private final Properties properties = new Properties(defaultProperties);

public static void main(String[] args) {
	new Main().start();
}

public Main() {
	frame = new JFrame();
	
	defaultProperties.setProperty("fromDir", "");
	defaultProperties.setProperty("toDir", "");
	defaultProperties.setProperty("axis", "x");
	defaultProperties.setProperty("length", "480");
	defaultProperties.setProperty("layer", "0");
	defaultProperties.setProperty("precision", "0");
	defaultProperties.setProperty("outputLayer", "0");
	
	try(var in = Files.newInputStream(Paths.get(PROPERTIES_PATH))) {
		properties.load(in);
	} catch(IOException e) {}
	logDisplay = new JLabel();
}

public void start() {
	addOperationButtons(addLogDisplay(addPropertyEditors(20)));
	
	frame.setSize(480, 360);
	frame.setTitle("Grab On Utilities");
	frame.setLayout(null);
	frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	frame.setVisible(true);
}

public int addPropertyEditors(int y) {
	for(var entry:defaultProperties.entrySet()) {
		String key = (String) entry.getKey();
		String value = (String) properties.get(key);
		String defaultValue = (String) entry.getValue();
		var label = new JLabel(key);
		label.setBounds(20, y, 80, 20);
		frame.add(label);
		var textBox = new JTextField(value == null ? defaultValue : value);
		textBox.setBounds(100, y, 100, 20);
		textBox.addActionListener(event -> {
			String text = textBox.getText();
			if(text.equals(defaultValue) ?
					properties.remove(key) != null :
					!text.equals(properties.setProperty(key, text))) try(var out = Files
					.newOutputStream(Paths.get(PROPERTIES_PATH))) {
				properties.store(out, null);
			} catch(IOException e) {}
		});
		frame.add(textBox);
		y += 25;
	}
	return y;
}

public int addLogDisplay(int y) {
	logDisplay.setBounds(40, y, 480, 20);
	frame.add(logDisplay);
	return y + 25;
}

public int addOperationButtons(int y) {
	for(int i = 0; i < buttonNames.length; i++) {
		var button = new JButton(buttonNames[i]);
		button.setBounds(20, y, 250, 20);
		int h = i;
		button.addActionListener(event -> {
			clearDisplay();
			try {
				buttonOperations[h].actionPerformed(event);
			} catch(RuntimeException e) {
				display(e);
				e.printStackTrace();
			}
		});
		frame.add(button);
		y += 25;
	}
	return y;
}

private final String[] buttonNames = {
	"Add stex png files",
	"Resize pngs",
	"Delete empty pngs",
	"Create polygons from svgs",
	"Move pngs"
};
private final ActionListener[] buttonOperations = {
	event -> {
		Path toDir = toDir();
		try {
			for(Path path:(Iterable<Path>) Files.list(fromDir()).filter(path -> !Files.isDirectory(path))::iterator) {
				String fileName = path.getFileName().toString();
				if(fileName.endsWith(".stex")) {
					int pngIndex = fileName.indexOf(".png");
					if(pngIndex >= 0) Files.copy(path, toDir.resolve(fileName.substring(0, pngIndex) + ".stex"),
							StandardCopyOption.REPLACE_EXISTING);
				}
			}
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	},
	event -> new PngResizer(this, getInt("length"), getInt("layer"), getAxis()).run(),
	event -> {
		try {
			paths:
			for(Path path:(Iterable<Path>) Files.list(toDir())
					.filter(path -> !Files.isDirectory(path)
							&& path.getFileName().toString().endsWith(".png"))
					::iterator) {
				try(var r = new PngReader(PngResizer.openRead(path))) {
					for(int i = 0; i < r.imgInfo.rows; i++)
						for(int h:((ImageLineInt) r.readRow()).getScanline())
							if(h != 0) continue paths;
				}
				Files.delete(path);
			}
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	},
	event -> createPolygonsFromSvgs(fromDir(), toDir(), getInt("layer"), getInt("precision")),
	event -> movePngs(fromDir(), toDir(), getInt("layer"), getInt("outputLayer"))
};

public void createPolygonsFromSvgs(Path fromDir, Path toDir, int layer, int precision) {
	try {
		var polygonPath = toDir.resolve("polygons.txt");
		var continuing = new Object() {
			boolean value;
		};
		Properties mapProperties = new Properties();
		try(var read = Files.newBufferedReader(fromDir.resolve("map.properties"))) {
			mapProperties.load(read);
		}
		int imageWidth;
		int imageHeight;
		try {
			String[] layerInfo = mapProperties.getProperty("map.imageLayers").split(";");
			String[] selectedLayer = layerInfo[layer].split(",");
			imageWidth = Integer.parseInt(selectedLayer[1]);
			imageHeight = Integer.parseInt(selectedLayer[2]);
		} catch(RuntimeException e) {
			throw new IllegalArgumentException("Map layer " + layer + " not found", e);
		}
		continuing.value = Files.exists(polygonPath);
		try(var out = Files.newBufferedWriter(polygonPath, StandardOpenOption.CREATE, StandardOpenOption.APPEND)) {
			for(PathContainer path:(Iterable<PathContainer>) Files.list(fromDir)
					.filter(path -> !Files.isDirectory(path) && path.getFileName().toString().endsWith(".svg"))
					.map(path -> new PathContainer(path, getImageNameInts(path.getFileName().toString())))
					.filter(path -> path.pos.length == 3 && path.pos[0] == layer)
					::iterator) try(var in = new BufferedInputStream(Files.newInputStream(path.p))) {
				var parser = SAXParserFactory.newInstance().newSAXParser();
				parser.parse(in, new DefaultHandler() {
					final BigDecimal xOffset = new BigDecimal(path.pos[1] * imageWidth);
					final BigDecimal yOffset = new BigDecimal(path.pos[2] * imageHeight);
					BigDecimal widthRatio;
					BigDecimal heightRatio;
					
					@Override
					public void startElement(String uri, String localName,
							String qName, Attributes attributes) throws SAXException {
						switch(qName) {
						case "path" -> {
							List<List<BigDecimal>> polygons = new ArrayList<>();
							var data = attributes.getValue("d");
							int start = 1;
							int end = 0;
							char c = data.charAt(0);
							List<BigDecimal> numbers = new ArrayList<>();
							do {
								do c = data.charAt(++end);
								while(c != 'M' && c != 'L' && c != 'Z' && end + 1 != data.length());
								String[] point = data.substring(start, end).split(" ", 2);
								numbers.add(new BigDecimal(point[0]));
								numbers.add(new BigDecimal(point[1]));
								start = end + 1;
								if(c == 'Z') {
									if(end + 1 == data.length()) break;
									do c = data.charAt(++end);
									while(c != 'M' && c != 'L' && c != 'Z' && end + 1 != data.length());
									start = end + 1;
									if(start < data.length()) {
										polygons.add(numbers);
										numbers = new ArrayList<>();
									}
									if(c == 'L') {
										numbers.add(new BigDecimal(point[0]));
										numbers.add(new BigDecimal(point[1]));
									}
								}
							} while(end + 1 < data.length());
							polygons.add(numbers);
							
							String transform = attributes.getValue("transform");
							transformBlock:
							if(transform != null) {
								if(transform.startsWith("matrix")) {
									String[] split = transform.substring(7, transform.length() - 1).split(" ");
									matrixBlock:
									if(split.length == 6) {
										for(int i = 1; i < 3; i++)
											if(!BigDecimal.ZERO.equals(new BigDecimal(split[i])))
												break matrixBlock;
										BigDecimal cent = new BigDecimal("0.01");
										for(int i = 0; i <= 3; i += 3)
											if(cent.compareTo(new BigDecimal(split[i]).subtract(BigDecimal.ONE).abs()) < 0)
												break matrixBlock;
										transform = "translate("
												+ (new BigDecimal(split[4]).abs().compareTo(cent) < 0 ? "0" : split[4])
												+ ','
												+ (new BigDecimal(split[5]).abs().compareTo(cent) < 0 ? "0" : split[5])
												+ ')';
									}
								}
								if(transform.startsWith("translate")) {
									String[] split = transform.substring(10, transform.length() - 1).split(",");
									if(split.length == 2) {
										BigDecimal x = new BigDecimal(split[0].trim()),
												y = new BigDecimal(split[1].trim());
										for(var list:polygons) for(int i = 0, j = list.size(); i < j; i++) {
											list.set(i, list.get(i).add(x));
											i++;
											list.set(i, list.get(i).add(y));
										}
										break transformBlock;
									}
								}
								display("Transform not supported: \"" + transform
										+ "\" - only \"translate(x, y)\" is supported");
							}
							
							try {
								for(var list:polygons) {
									if(continuing.value) out.write('\n');
									else continuing.value = true;
									out.write(list.get(0)
											.multiply(widthRatio)
											.add(xOffset)
											.movePointRight(precision)
											.setScale(0, RoundingMode.HALF_UP)
											.movePointLeft(precision)
											.toPlainString());
									out.write(',');
									out.write(list.get(1)
											.multiply(heightRatio)
											.add(yOffset)
											.movePointRight(precision)
											.setScale(0, RoundingMode.HALF_UP)
											.movePointLeft(precision)
											.toPlainString());
									for(int i = 2, j = list.size(); i < j; i++) {
										out.write(',');
										out.write(list.get(i++)
												.multiply(widthRatio)
												.add(xOffset)
												.movePointRight(precision)
												.setScale(0, RoundingMode.HALF_UP)
												.movePointLeft(precision)
												.toPlainString());
										out.write(',');
										out.write(list.get(i)
												.multiply(heightRatio)
												.add(yOffset)
												.movePointRight(precision)
												.setScale(0, RoundingMode.HALF_UP)
												.movePointLeft(precision)
												.toPlainString());
									}
								}
							} catch (IOException e) {
								throw new UncheckedIOException(e);
							}
						} case "svg" -> {
							String width = attributes.getValue("width"),
									height = attributes.getValue("height");
							if(width.length() > 2 && width.substring(width.length() - 2).equals("pt"))
								width = width.substring(0, width.length() - 2);
							if(height.length() > 2 && height.substring(height.length() - 2).equals("pt"))
								height = height.substring(0, height.length() - 2);
							try {
								widthRatio = new BigDecimal(imageWidth).divide(new BigDecimal(width));
							} catch(ArithmeticException e) {
								widthRatio = new BigDecimal(imageWidth / Double.parseDouble(width));
							}
							try {
								heightRatio = new BigDecimal(imageHeight).divide(new BigDecimal(height));
							} catch(ArithmeticException e) {
								heightRatio = new BigDecimal(imageHeight / Double.parseDouble(height));
							}
						}
						}
					}
				});
			} catch(ParserConfigurationException | SAXException e) {
				throw new IOException(e);
			}
		}
	} catch (IOException e) {
		throw new UncheckedIOException(e);
	}
}

public void movePngs(Path fromDir, Path toDir, int layer, int outputLayer) {
	String layerString = String.valueOf(layer);
	String outputLayerString = String.valueOf(outputLayer);
	try {
		for(Path path:(Iterable<Path>) Files.list(fromDir)
				.filter(path -> {
					if(Files.isDirectory(path)) return false;
					String name = path.getFileName().toString();
					return name.startsWith(layerString) && name.endsWith(".png");
				})
				::iterator)
			Files.copy(path,
					toDir.resolve(outputLayerString + path.getFileName().toString().substring(layerString.length())),
					StandardCopyOption.REPLACE_EXISTING);
	} catch (IOException e) {
		throw new UncheckedIOException(e);
	}
}

public Path fromDir() {
	return Paths.get(properties.getProperty("fromDir"));
}

public Path toDir() {
	return Paths.get(properties.getProperty("toDir"));
}

public int getInt(String key) {
	String value = properties.getProperty(key);
	if(value == null) throw new IllegalArgumentException("No value for " + key);
	try {
		return Integer.parseInt(value);
	} catch(NumberFormatException e) {
		throw new IllegalArgumentException("Key " + key + " has non-integer value " + value);
	}
}

public boolean getAxis() {
	return switch(properties.getProperty("axis")) {
	case "x" -> false;
	case "y" -> true;
	default -> throw new IllegalArgumentException("axis should be either \"x\" or \"y\"");
	};
}

public void display(String text) {
	System.out.println(text);
	logDisplay.setText(text);
}

public void display(Object text) {
	String str = text == null ? "null" : text.toString();
	System.out.println(str);
	logDisplay.setText(str);
}

public void clearDisplay() {
	logDisplay.setText("");
}

public static int[] getImageNameInts(String name) {
	String[] strs = name.substring(0, name.lastIndexOf('.')).split("_");
	try {
		int[] ints = new int[strs.length];
		for(int i = 0; i < strs.length; i++) ints[i] = Integer.parseInt(strs[i]);
		return ints;
	} catch(NumberFormatException e) {
		return new int[0];
	}
}

public static Path getPath(Path dir, int[] position, String extension) {
	return dir.resolve(position[0] + "_" + position[1] + "_" + position[2] + extension);
}

public static int compare(int[] pos1, int[] pos2, int[] indexOrder) {
	int l = indexOrder.length - 1;
	for(int i = 0; i < l; i++) {
		int d = pos1[indexOrder[i]] - pos2[indexOrder[i]];
		if(d != 0) return d;
	}
	return pos1[indexOrder[l]] - pos2[indexOrder[l]];
}

}