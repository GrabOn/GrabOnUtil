package magnileve.grabonutil;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

import ar.com.hjg.pngj.ImageInfo;
import ar.com.hjg.pngj.ImageLineInt;
import ar.com.hjg.pngj.PngReader;
import ar.com.hjg.pngj.PngWriter;

public class PngResizer implements Runnable {

private static final int[] COMPARE_ORDER_X = {0, 2, 1};
private static final int[] COMPARE_ORDER_Y = {0, 1, 2};

private final Main main;
private final int length;
private final int layer;
private final boolean axis;

public PngResizer(Main main, int length, int layer, boolean axis) {
	this.main = main;
	this.length = length;
	this.layer = layer;
	this.axis = axis;
}

@Override
public void run() {
	Path toDir = main.toDir();
	PathContainer[] paths;
	try {
		for(Path path:(Iterable<Path>) Files.list(toDir)
				.filter(path -> {
					if(Files.isDirectory(path)) return false;
					else {
						int[] pos = Main.getImageNameInts(path.getFileName().toString());
						return pos.length == 3 && pos[0] == layer;
					}
				})::iterator)
			Files.delete(path);
		paths = Files.list(main.fromDir())
				.filter(path -> !Files.isDirectory(path))
				.map(path -> {
					String name = path.getFileName().toString();
					return name.endsWith(".png") ? new PathContainer(path, Main.getImageNameInts(name)) : null;
				})
				.filter(path -> path != null && path.pos.length == 3 && path.pos[0] == layer)
				.toArray(length -> new PathContainer[length]);
	} catch(IOException e) {
		throw new UncheckedIOException(e);
	}
	
	Arrays.sort(paths, (p1, p2) -> Main.compare(p1.pos, p2.pos, axis ? COMPARE_ORDER_Y : COMPARE_ORDER_X));
	if(paths.length == 0) return;
	
	var tempReader = new PngReader(paths[0].p.toFile());
	ImageInfo rImgInfo = tempReader.imgInfo;
	tempReader.close();
	int rWidth = rImgInfo.cols;
	int rHeight = rImgInfo.rows;
	int wWidth;
	int wHeight;
	if(axis) {
		wWidth = rWidth;
		wHeight = length;
	} else {
		wWidth = length;
		wHeight = rHeight;
	}
	ImageInfo wImgInfo = rImgInfo.withSize(wWidth, wHeight);
	PathContainer rImage = paths[0];
	int pathIndex = 1;
	
	do {
		int[] rPos = rImage.pos.clone();
		int rIndex = rPos[axis ? 2 : 1];
		int wIndex = axis ? rIndex * rHeight / wHeight : rIndex * rWidth / wWidth;
		int currentSection = rPos[axis ? 1 : 2];
		int offset = axis ? rIndex * rHeight % wHeight : rIndex * rWidth % wWidth;
		if(offset < 0) {
			wIndex--;
			offset += axis ? wHeight : wWidth;
		}
		
		if(axis ? rHeight <= wHeight : rWidth <= wWidth) {
			int[][] data = new int[wHeight][wWidth];
			int[][] nextData = new int[wHeight][wWidth];
			
			while(rImage != null && rImage.pos[axis ? 1 : 2] == currentSection) {
				if(rIndex == rImage.pos[axis ? 2 : 1])
					try(var read = new PngReader(openRead(rImage.p))) {
						if(read.imgInfo.bitDepth == 8 && read.imgInfo.channels >= 3) {
					//if reaching end of output png
					if(axis ? offset + rHeight >= wHeight : offset + rWidth >= wWidth) {
						
						//store input png and transfer overlap
						if(axis) {
							for(int h = offset; h < wHeight; h++)
								data[h] = mergeBytes(((ImageLineInt) read.readRow()).getScanline());
							for(int h = 0, offsetLength = rHeight + offset - wHeight; h < offsetLength; h++)
								nextData[h] = mergeBytes(((ImageLineInt) read.readRow()).getScanline());
						} else {
							int copyLength = wWidth - offset;
							for(int h = 0; h < rHeight; h++) {
								int[] row = mergeBytes(((ImageLineInt) read.readRow()).getScanline());
								System.arraycopy(row, 0, data[h], offset, copyLength);
								System.arraycopy(row, copyLength, nextData[h], 0, rWidth - copyLength);
							}
						}
						
						//write new png
						rPos[axis ? 2 : 1] = wIndex++;
						writePngAndClear(toDir, rPos, wImgInfo, data);
						int[][] clearedData = data;
						data = nextData;
						nextData = clearedData;
						
						offset += axis ? rHeight - wHeight : rWidth - wWidth;
					} else {
						//store input png
						if(axis) for(int h = offset; h < offset + rHeight; h++)
							data[h] = mergeBytes(((ImageLineInt) read.readRow()).getScanline());
						else for(int h = 0; h < rHeight; h++)
							System.arraycopy(mergeBytes(((ImageLineInt) read.readRow()).getScanline()), 0,
									data[h], offset, rWidth);
						offset += axis ? rHeight : rWidth;
					}
					
					rIndex++;
					rImage = pathIndex == paths.length ? null : paths[pathIndex++];
					continue;
				}}
				offset += axis ? rHeight : rWidth;
				if(offset >= (axis ? wHeight : wWidth)) {
					//write new png
					rPos[axis ? 2 : 1] = wIndex++;
					writePngAndClear(toDir, rPos, wImgInfo, data);
					int[][] clearedData = data;
					data = nextData;
					nextData = clearedData;
					
					offset -= axis ? wHeight : wWidth;
				}
				rIndex++;
			}
			if(offset != 0) {
				//write new png from remaining overlap
				rPos[axis ? 2 : 1] = wIndex++;
				writePng(toDir, rPos, wImgInfo, data);
			}
		} else {
			int[][] data = new int[rHeight][rWidth];
			//store input png
			try(var read = new PngReader(openRead(rImage.p))) {
				if(read.imgInfo.bitDepth == 8 && read.imgInfo.channels >= 3)
					for(int h = 0; h < rHeight; h++)
						data[h] = mergeBytes(((ImageLineInt) read.readRow()).getScanline());
				rIndex++;
				rImage = pathIndex == paths.length ? null : paths[pathIndex++];
			}
			
			//store data after image start
			int[][] overlapData1 = new int[wHeight][wWidth];
			if(axis) for(int i = 0, h = offset; h < wHeight; i++, h++)
				overlapData1[h] = data[i];
			else for(int h = 0; h < wHeight; h++)
				System.arraycopy(data[h], 0, overlapData1[h], offset, wWidth - offset);
			
			//write new png
			rPos[axis ? 2 : 1] = wIndex++;
			writePng(toDir, rPos, wImgInfo, overlapData1);
			offset = (axis ? wHeight : wWidth) - offset;
			
			while(rImage != null && rImage.pos[axis ? 1 : 2] == currentSection) {
				//while no overlap
				while(axis ? offset + wHeight < rHeight : offset + wWidth < rWidth) {
					//write new png
					rPos[axis ? 2 : 1] = wIndex++;
					writePng(toDir, rPos, wImgInfo, axis ?
							subarray(data, offset, wHeight) :
							subarrays(data, offset, wWidth));
					offset += axis ? wHeight : wWidth;
				}
				
				//store data before overlap
				int[][] overlapData = new int[wHeight][wWidth];
				int copyLength = (axis ? rHeight : rWidth) - offset;
				if(axis) for(int i = 0, h = offset; i < copyLength; i++, h++)
					overlapData[i] = data[h].clone();
				else for(int h = 0; h < wHeight; h++)
					System.arraycopy(data[h], offset, overlapData[h], 0, copyLength);
				
				//store input png
				clear(data);
				if(rIndex == rImage.pos[axis ? 2 : 1]) {
					try(var read = new PngReader(openRead(rImage.p))) {
						if(read.imgInfo.bitDepth == 8 && read.imgInfo.channels >= 3) 
							for(int h = 0; h < rHeight; h++)
								data[h] = mergeBytes(((ImageLineInt) read.readRow()).getScanline());
					}
					rImage = pathIndex == paths.length ? null : paths[pathIndex++];
				}
				rIndex++;
				
				//store data after overlap
				if(axis) for(int i = 0, h = copyLength; h < wHeight; i++, h++)
					overlapData[h] = data[i];
				else for(int h = 0; h < wHeight; h++)
					System.arraycopy(data[h], 0, overlapData[h], copyLength, wWidth - copyLength);
				
				//write new png
				rPos[axis ? 2 : 1] = wIndex++;
				writePng(toDir, rPos, wImgInfo, overlapData);
				offset += axis ? wHeight - rHeight : wWidth - rWidth;
			}
			//while no overlap
			while(axis ? offset + wHeight < rHeight : offset + wWidth < rWidth) {
				//write new png
				rPos[axis ? 2 : 1] = wIndex++;
				writePng(toDir, rPos, wImgInfo, axis ?
						subarray(data, offset, wHeight) :
						subarrays(data, offset, wWidth));
				offset += axis ? wHeight : wWidth;
			}
			//store data before image end
			int[][] overlapData = new int[wHeight][wWidth];
			int copyLength = (axis ? rHeight : rWidth) - offset;
			if(axis) for(int i = 0, h = offset; i < copyLength; i++, h++)
				overlapData[i] = data[h];
			else for(int h = 0; h < wHeight; h++)
				System.arraycopy(data[h], offset, overlapData[h], 0, copyLength);
			
			//write new png
			rPos[axis ? 2 : 1] = wIndex++;
			writePng(toDir, rPos, wImgInfo, overlapData);
		}
	} while(rImage != null);
}

public static void clear(int[][] array) {
	for(int h = 0; h < array.length; h++) {
		int[] row = array[h];
		for(int w = 0; w < row.length; w++) row[w] = 0;
	}
}

public static void writePng(Path toDir, int[] pos, ImageInfo imgInfo, int[][] data) {
	var write = new PngWriter(openWrite(Main.getPath(toDir, pos, ".png")), imgInfo);
	try {
		for(int h = 0; h < data.length; h++) write.writeRowInt(separateBytes(data[h]));
	} catch(Throwable t) {
		write.close();
		throw t;
	}
	write.end();
}

public static void writePngAndClear(Path toDir, int[] pos, ImageInfo imgInfo, int[][] data) {
	writePng(toDir, pos, imgInfo, data);
	clear(data);
}

public static int[][] subarrays(int[][] array, int offset, int length) {
	int[][] subarrays = new int[array.length][];
	for(int h = 0; h < subarrays.length; h++) subarrays[h] = subarray(array[h], offset, length);
	return subarrays;
}

public static int[] subarray(int[] array, int offset, int length) {
	int[] subarray = new int[length];
	System.arraycopy(array, offset, subarray, 0, length);
	return subarray;
}

public static int[][] subarray(int[][] array, int offset, int length) {
	int[][] subarray = new int[length][];
	System.arraycopy(array, offset, subarray, 0, length);
	return subarray;
}

public static int[] mergeBytes(int[] bytes) {
	int[] ints = new int[bytes.length / 4];
	int i = 0, h = 0;
	while(i < ints.length) ints[i++] = bytes[h++] << 24
			| bytes[h++] << 16
			| bytes[h++] << 8
			| bytes[h++];
	return ints;
}

public static int[] separateBytes(int[] ints) {
	int[] bytes = new int[ints.length * 4];
	int i = 0, h = 0;
	while(i < ints.length) {
		int j = ints[i++];
		bytes[h++] = j >>> 24 & 0xFF;
		bytes[h++] = j >>> 16 & 0xFF;
		bytes[h++] = j >>> 8 & 0xFF;
		bytes[h++] = j & 0xFF;
	}
	return bytes;
}

public static InputStream openRead(Path path) {
	try {
		return new BufferedInputStream(Files.newInputStream(path));
	} catch (IOException e) {
		throw new UncheckedIOException(e);
	}
}

public static OutputStream openWrite(Path path) {
	try {
		return new BufferedOutputStream(Files.newOutputStream(path));
	} catch (IOException e) {
		throw new UncheckedIOException(e);
	}
}

}